/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @class FCALConstruction
 *
 * @brief Insert the LAr FCAL into a pre-defined mother volume.
 *
 * @author Joe Boudreau August 2004
 */

#ifndef LARGEOFCAL_FCALCONSTRUCTION_H
#define LARGEOFCAL_FCALCONSTRUCTION_H

#include "GeoModelKernel/GeoFullPhysVol.h"
#include "RDBAccessSvc/IRDBAccessSvc.h"

// Forward declarations
class ISvcLocator;


namespace LArGeo {

  class FCALConstruction 
  {
  public:

    FCALConstruction() = default;
    ~FCALConstruction() = default;

    FCALConstruction (const FCALConstruction &) = delete;
    FCALConstruction & operator= (const FCALConstruction &) = delete;

    // Get the envelope containing this detector.
    GeoIntrusivePtr<GeoVFullPhysVol> GetEnvelope(bool bPos);

    // Set a limit on cell number (for Visualization only);
    void setFCALVisLimit(int maxCell) {m_VisLimit    = maxCell;}

    // Set fullGeo flag
    void setFullGeo(bool flag) {m_fullGeo = flag;}

  private: 
    // volumes that are private member variables:
    GeoIntrusivePtr<GeoFullPhysVol>  m_fcalPhysical{};

    // full physical volumes for absorbers
    GeoIntrusivePtr<GeoFullPhysVol>  m_absPhysical1{};
    GeoIntrusivePtr<GeoFullPhysVol>  m_absPhysical2{};  
    GeoIntrusivePtr<GeoFullPhysVol>  m_absPhysical3{};

    int m_VisLimit{0};

    ISvcLocator*         m_svcLocator{nullptr};
    IRDBRecordset_ptr    m_fcalMod;
    IRDBRecordset_ptr    m_LArPosition;

    bool             m_fullGeo{true};  // true->FULL, false->RECO
  };

}  // namespace LArGeo

#endif // __FCALConstruction_H__
