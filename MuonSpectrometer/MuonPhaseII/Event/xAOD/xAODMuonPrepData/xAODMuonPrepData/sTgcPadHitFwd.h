/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODMUONPREPDATA_sTgcPadHitFWD_H
#define XAODMUONPREPDATA_sTgcPadHitFWD_H
#include "xAODMuonPrepData/sTgcMeasurementFwd.h"
/** @brief Forward declaration of the xAOD::sTgcPadHit */
namespace xAOD{
   class sTgcPadHit_v1;
   using sTgcPadHit = sTgcPadHit_v1;

   class sTgcPadAuxContainer_v1;
   using sTgcPadAuxContainer = sTgcPadAuxContainer_v1;
}
#endif
