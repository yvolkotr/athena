/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

#include "JetUtils/JetCaloCalculations.h"
#include "JetUtils/JetCaloQualityUtils.h"
